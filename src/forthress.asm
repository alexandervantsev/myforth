global _start

%include "macro.inc"
%include "util.inc"

%define pc r13
%define w r14
%define rstack r15

section .bss
rs: 		resq 65536
u_mem: 		resq 65536
dict: 		resq 65536
word_buffer: 	resb 1024
state: 		resq 1		; 1 - compile mode

%include "words.inc"

section .data
last_word: 	dq _lw
rsp_b: 		dq 0
no_word: 	db ' : There is no such word', 10, 0
here: 		dq dict
xt_exit: 	dq exit
program_stub: 	dq 0
xt_interpreter: dq .interpreter
.interpreter: 	dq interpreter_loop

section .text

_start:
	mov rstack, rs + 65536 * 8
	mov [rsp_b], rsp
    	mov pc, xt_interpreter
    	jmp next

interpreter_loop:
    	cmp qword [state], 0
    	jne compiler_loop

	mov rdi, word_buffer
	call read_word		   			            		 
	call find_word	
	test rax, rax
	jz .parse_number

	mov rdi, rax
	call cfa

	mov [program_stub], rax
	mov pc, program_stub
	jmp next
.parse_number:
	mov rdi, word_buffer
	call parse_int

	test rdx, rdx
	jz .no_word

	push rax	
	mov pc, xt_interpreter
	jmp next
.no_word:
	call print_no_word
	mov pc, xt_interpreter
	jmp next


compiler_loop:
    	cmp qword [state], 1      	
    	jne interpreter_loop

    	mov rdi, word_buffer
    	call read_word              	
    	call find_word              	
	test rax, rax
    	jz .parse_number	   
    	mov rdi, rax
    	call cfa		        
    	cmp byte [rax - 1], 1		; check as immediate
    	je .immediate               
	
    	mov rdi, [here]			; else add xt to here
   	mov [rdi], rax              	
   	add qword [here], 8
    	jmp compiler_loop
.parse_number:
    	mov rdi, word_buffer
    	call parse_int

    	test rdx, rdx
    	jz .no_word

	mov rdi, [here]
    	cmp qword [rdi - 8], xt_branch
	je .branch
	cmp qword [rdi - 8], xt_branch0
	je .branch

    	mov rdi, [here]	
    	mov qword [rdi], xt_lit     	
    	add qword [here], 8
.branch:
	mov rdi, [here]	
	mov [rdi], rax
	add qword [here], 8
	jmp compiler_loop
.immediate:
    	mov [program_stub], rax
    	mov pc, program_stub
    	jmp next
.no_word:
	call print_no_word
    	mov pc, xt_interpreter
	jmp next

next:
    	mov w, pc
    	add pc, 8
    	mov w, [w]
    	jmp [w]	

docol:
	sub rstack, 8		
	mov [rstack], pc
	add w, 8	    	
	mov pc, w	    	
	jmp next	    	

exit:
	mov pc, [rstack]
	add rstack, 8		
	jmp next	    	
