: IMMEDIATE  last_word_addr @ cfa 1 - dup c@ 1 or swap c! ;

: over r> dup >r r> swap ;

: if ' branch0 , here_addr 0  , ; IMMEDIATE
: else ' branch , here_addr 0 , swap here_addr swap !  ; IMMEDIATE
: then here_addr swap ! ; IMMEDIATE

: repeat here_addr ; IMMEDIATE
: until  ' branch0 , , ; IMMEDIATE


: for ' swap , ' >r dup , , here_addr  ' r> dup , , 
' over over , ' >r dup , , ' < ,  ' branch0 ,  
here_addr 0 , swap ; IMMEDIATE

: endfor ' r> , ' lit , 1 , ' + , ' >r , ' branch , ,  here_addr swap ! 
' r> ' drop over over , , ;  IMMEDIATE